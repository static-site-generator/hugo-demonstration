+++
title = 'Installation'
date = 2023-09-18T11:34:24+02:00
draft = false
weight = 1
+++

## Hugo Binary

### Installation

{{< tabs >}}

{{% tab title="Linux" style="orange" %}}
1. Powershell öffnen
2. Command ausführen: ```sudo apt install hugo```
3. Powershell neustarten
{{% /tab %}}

{{% tab title="Windows" color="lightblue" %}}
1. Terminal öffnen
2. Command ausführen: ```winget install Hugo.Hugo.Extended```
3. Terminal neustarten
{{% /tab %}}

{{< /tabs >}}


### Offizielle Seite oder so

1. Gehe auf die Seite [gohugo.io/installation/](https://gohugo.io/installation/)
2. Wähle dein OS oder so aus
3. Führe die Commands im Terminal aus

![Download Image](https://images.easytechjunkie.com/computer-downloading-status-bar.jpg)