+++
title = 'Aufgabe 2 (Fortgeschritten)'
date = 2023-09-18T11:28:14+02:00
draft = false
weight = 2
+++

## Auf GitLab hosten
Finde heraus, wie man die Webseite auf GitLab hostet (mit ```.gitlab-ci.yml```).

## Theme ändern
Ändere den Theme deiner Webseite. ([Themes](https://themes.gohugo.io/))

## Customize
Ändere Farben, Logo oder Layout der Webseite.

## Video anzeigen
Finde heraus, wie man ein Video (```.mp4```) oder ein Youtube Video anzeigen lassen kann.

## Ausprobieren
Versuche eigene Ideen umzusetzen oder finde neues heraus.

![Good Luck](https://media.tenor.com/-TdVdx6CgOsAAAAC/good-luck-peace.gif)