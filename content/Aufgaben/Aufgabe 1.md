+++
title = 'Aufgabe 1'
date = 2023-09-18T11:28:12+02:00
draft = false
weight = 1
+++

## Aufgabenstellung: Anpassung der Hugo-Website mit Relearn

Deine Aufgabe ist es, eine Hugo-Website zu erstellen oder zu aktualisieren und dabei die Hauptseite anzupassen, um ein Bild und den Text "Hello, World!" anzuzeigen. Hier ist eine verkürzte Version der Anweisungen:

### Schritt 1 - Projektverzeichnis erstellen
Erstelle ein neues Hugo-Projekt oder arbeite mit einem vorhandenen Projekt.
### Schritt 2 - Hauptseite erstellen
Erstelle eine neue Hauptseite mit dem Befehl hugo new _index.md.

### Schritt 3 - Inhalt hinzufügen
Öffne die _index.md-Datei im content-Verzeichnis und füge den folgenden Inhalt ein:

```
+++
title = 'Startseite'
+++

![Ein Bild](/pfad/zum/bild.jpg)

# Hello, World!
```

### Schritt 4 - Hugo-Server starten
Starte den Hugo-Entwicklungsserver mit dem Befehl hugo server.

### Schritt 5 - Überprüfung
Öffne deinen Webbrowser und besuche http://localhost:1313/, um sicherzustellen, dass das Bild und der Text auf der Startseite korrekt angezeigt werden. 

![checklist](https://media.tenor.com/Mos3BAm5aTcAAAAM/checklist-task.gif)