+++
title = 'Basics'
date = 2023-09-18T14:31:40+02:00
draft = false
weight = 1
+++

## Projekt ersellen
1. Öffne das Terminal
2. Gehe zum Verzeichnis wo das Projektverzeichnis erstellt werden soll.
3. Führe ```hugo new site <Projektname>``` aus.

So sieht das neu erstellte Projekt aus:

```
.
├── archetypes/
├── assets/
├── content/         {{% icon arrow-left %}} Hier kommt der Inhalt (.md files)
├── data/
├── i18n/
├── layouts/
├── static/
├── themes/          {{% icon arrow-left %}} Hier kommen die Themes rein
└── hugo.toml        {{% icon arrow-left %}} Hier sind die Konfigurationen gespeichert
```

![Pooh](https://media4.giphy.com/media/o0vwzuFwCGAFO/200w.gif?cid=6c09b952tqmk2qvkobr2bwlsceasryk2ykvwc5tssj0o4yls&ep=v1_gifs_search&rid=200w.gif&ct=g)

## Seiten erstellen
Eine Seite kann mit ```hugo new <filename>.md``` erstellt werden. Die Seite wird im Verzeichnis ```/content/``` erstellt.
 - Der Index muss ```_index.md``` heissen. Beispiel: ```hugo new _index.md```
 - Man können auch mehrere Seite in einem Verzeichnis erstellt werden. Beispiel: ```hugo new Kapitel1/Seite1.md``` oder ```hugo new Kapitel1/_index.md```.
 - Jedes Verzeichnis braucht ein Index.

### So könnte eine Struktur aussehen
```
content/
├── _index.md
├── Kapitel1/
│   ├── _index.md
│   ├── Seite1.md
│   └── Seite2.md
└── Kapitel2/
    ├── _index.md
    └── Seite1.md
```
oder in diesem Projekt
```
content/
├── Aufgaben/
│   ├── _index.md
│   ├── Aufgabe1.md
│   └── Aufgabe2.md
├── Theorie/
│   ├── _index.md
│   ├── Basics.md
│   ├── Build.md
│   └── Themes.md
├── _index.md
└── installation.md
```

{{% notice style="note" %}}
Die Reihenfolge der Seiten werden mit `weight` festgelegt.
{{% /notice %}}

## Seite bearbeiten
Wenn eine Seite mit hilfe eines Commands erstellt wird, wird in der Datei folgendes angezeigt:
{{< highlight lineNos="true" type="md" wrap="true" >}}
+++
title = 'Titel'
date = 2023-09-18T11:32:07+02:00
draft = true
+++
{{< /highlight >}}

- ```title``` ist der Titel, der z.B Links beim Menu angezeigt wird
- ```date``` gibt die Zeit an, wann es erstellt wurde.
- ```draft``` sollte auf ```false``` gesetzt werden, damit die Seite später auf GitLab pages auch angezeigt werden soll. 
- ```weight``` gibt an, in welcher Reihenfolge die Seite links im Menu angezeigt wird. Je kleiner die Zahl, desto weiter oben wird die Seite angezeigt.
    \
    Beispiel: 
    ```
    weight = 1
    ```
- ```menuTitle ``` ist der Titel, der nur Links im Menu angezeigt wird.
    \
    Beispiel: 
    ```
    menuTitle = 'Banane'
    ```

Nun kann man unter dem Header mit Markdown schreiben. ([Markdown Syntax](https://www.markdownguide.org/basic-syntax/))

# "the only limit is your imagination"
![meme](https://media.tenor.com/7UarUv_Z1QYAAAAC/gunna-fire.gif)