+++
title = 'Themes'
date = 2023-09-18T13:00:37+02:00
draft = false
weight = 2
+++

## Was sind Themes?
Themes sind Sammlungen von Hugo-Layouts, die Ihnen die mühsame Erstellung Ihrer Website abnehmen.

## Wo kann ich Themes finden?
Viele und gute Themes kann man direkt bei [Hugo](https://themes.gohugo.io) finden.

## Wie lade ich ein Theme herunter?
1. Clicke auf Download (wird auf Github weitergleitet)![installation step 1](https://i.imgur.com/yp92R0H.png)

2. Klone das Repository im Verzeichnis `/themes/`
    ![Installation step 2](https://i.imgur.com/DSgQnfV.png)
    ```
    .
    ├── archetypes/
    ├── assets/
    ├── content/
    ├── data/
    ├── i18n/
    ├── layouts/
    ├── static/
    ├── themes/
    │   ├── hugo-theme1
    │   ├── hugo-theme2
    │   └── ...
    ├── .hugo_build.lock
    └── hugo.toml
    ```
3. Füge `hugo.toml` folgendes hinzu: `theme = '<Name des Ordnes im themes-verzeichnis>'`
    ![Installation](https://i.imgur.com/dVUFDI1.png)
\
\
\
{{% notice style="note" %}}
Je nach Theme muss der Ordnerstruktur geändert werden.
{{% /notice %}}
