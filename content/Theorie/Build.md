+++
title = 'Build / Deploy'
date = 2023-09-18T18:35:40+02:00
draft = false
weight = 3
+++

![Build](https://media.tenor.com/239F33W7-cIAAAAC/building-fortnite.gif)

## Lokaler Start der Website
Starten mit dem folgenden Befehl:
```
hugo serve
```
Gehe zu ```http://localhost:1313```

## Webseite Builden
Builde mit dem folgenden Befehl:
```
hugo
```
Es wird ein ```public``` Ordner erstellt, der alle statischen Inhalte und Assets für Ihre Website enthält. Sie kann nun auf jedem Webserver bereitgestellt werden.

## GitLab
1. Erstelle eine Datei mit dem namen `.gitlab-ci.yml`
2. Füge folgendes in die Datei ein:
{{< highlight lineNos="true" type="yml" wrap="true" title=".gitlab-ci.yml" >}}
variables:
  DART_SASS_VERSION: 1.64.1
  HUGO_VERSION: 0.115.4
  NODE_VERSION: 20.x
  GIT_DEPTH: 0
  GIT_STRATEGY: clone
  GIT_SUBMODULE_STRATEGY: recursive
  TZ: America/Los_Angeles

image:
  name: golang:1.20.6-bookworm

pages:
  script:
    # Install brotli
    - apt-get update
    - apt-get install -y brotli
    # Install Dart Sass
    - curl -LJO https://github.com/sass/dart-sass/releases/download/${DART_SASS_VERSION}/dart-sass-${DART_SASS_VERSION}-linux-x64.tar.gz
    - tar -xf dart-sass-${DART_SASS_VERSION}-linux-x64.tar.gz
    - cp -r dart-sass/ /usr/local/bin
    - rm -rf dart-sass*
    - export PATH=/usr/local/bin/dart-sass:$PATH
    # Install Hugo
    - curl -LJO https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_linux-amd64.deb
    - apt-get install -y ./hugo_extended_${HUGO_VERSION}_linux-amd64.deb
    - rm hugo_extended_${HUGO_VERSION}_linux-amd64.deb
    # Install Node.js
    - curl -fsSL https://deb.nodesource.com/setup_${NODE_VERSION} | bash -
    - apt-get install -y nodejs
    # Install Node.js dependencies
    - "[[ -f package-lock.json || -f npm-shrinkwrap.json ]] && npm ci || true"
    # Build
    - hugo --gc --minify
    # Compress
    - find public -type f -regex '.*\.\(css\|html\|js\|txt\|xml\)$' -exec gzip -f -k {} \;
    - find public -type f -regex '.*\.\(css\|html\|js\|txt\|xml\)$' -exec brotli -f -k {} \;
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
{{< /highlight >}}
3. Speichere und lade die Änderungen auf GitLab hoch.
4. Beim Pipeline sollte nun ein Job ausgeführt werden. Warte bis der Job/Pipeline oder so fertig ist.
5. Gehe nun auf Deployments und dann auf Pages. Der Link wird angezeigt.

{{% notice style="note" %}}
Im `hugo.toml` muss die `baseURL` passend zum Link geändert werden. Ansonsten funktioniert das CSS nicht oder die Webseite wird gar nicht angezeigt.
{{% /notice %}}