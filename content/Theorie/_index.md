+++
title = 'Theorie'
date = 2023-09-18T11:32:07+02:00
draft = false
weight = 3
+++

## Was ist eine statische Webseite?
Sie zeigen in der Regel allen Benutzern den gleichen Inhalt an. Sie verwenden serverseitiges Rendering, um HTML-, CSS- und Javascript-Dateien bereitzustellen.

## Was ist ein «statischer Webseite-Generator»(SSG)?
Es ist ein Software-Tool, das ein statische HTML Webseite generiert, indem es reine Textdateien verarbeitet, die den Inhalt und das Markup der Webseite enthalten.

Vorteile der Verwendung
-	Geschwindigkeit 
-	Keine Datenbank erforderlich {{% icon arrow-right %}} höhere Sicherheit
-	Skalierung einfach
-	Keine statische Dateien {{% icon arrow-right %}} bei Bedarf einfach zu migrieren

## Wie kreiere ich eine Statische GitLab Webseite?
Es gibt viele Wege um eine Statische Website auf GitLab zu hosten. Hier ist eine liste zu allen auf GitLab verfügbaren Pages: https://gitlab.com/pages 

## Hugo
Hugo ist das am meisten verbreitest dieser Tools auf GitLab. Es ist bassieren auf Go und sehr einfach zu benutzen. 
 Quick start | Hugo (gohugo.io)
In diesem Teil wird erklärt wie man eine Hugo Website lokal erstellen kann. Wenn man das erfolgreich abgeschlossen hat, kann man mit Hilfe dieser Dokumentation, die Website auf Gitlab hosten.
https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/

![Learn](https://media.tenor.com/rVTG4HW7d_AAAAAd/studying-absorb-knowledge.gif)