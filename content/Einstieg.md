+++
title = 'Einstieg'
date = 2023-09-19T00:11:26+02:00
draft = false
weight = 2
+++

## Video
Hier ist eine kleine Zusammenfassung (ca. 10min) wie man eine einfache Webseite mit Hugo erstellt.
\
[Link zum Video](https://www.youtube.com/watch?v=u2E4XVvygtI)
[![Image](https://img.youtube.com/vi/u2E4XVvygtI/0.jpg)](https://www.youtube.com/watch?v=u2E4XVvygtI)